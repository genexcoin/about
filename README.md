# Genexcoin overview #
### About Gix/G1X ###

The intention behind Genexcoin is to create a currency that is easily tradable between individuals, where the primary objective is exchange and not mining, for this it is necessary to bring the network to a state of centralization where a minimum amount of low-resources computers are responsible for counting the blocks, charging a minimal commission and completely avoiding the unnecessary connection of thousands of sophisticated devices, consuming an enormous amount of resources to extract a minimum profit.

By the time it goes public, all of the Gix / G1X blocks will have been mined.

The main unit of Genexcoin is the Gix, understanding that 100,000,000 of Gix are equivalent to a G1X so that this is easy to interpret numerically, without this representing arbitrarily creating a coin of bulky value in G1X.

As a speculative control method, the value of a Gix has been set at USD0.0001 for the date of November 2020, but to determine its real value, two more elements that represent the same amount for the same period of time may be taken into account. . The value of a Gix will be recalculated and published every 3 months taking into account three elements of value.

### Principle of no speculation ###
Genexcoin has an issuance of 40,000,000 G1X that have been mined by the creator but cannot be traded by it, the intention of this is to avoid speculation and be able to give this cryptoactive a value that cannot be used by the creator himself. Despite the fact that the creator does not own Genexcoin, he will be in charge of safeguarding and distributing it. But what does the creator gain from all this? When Genexcoin is made public for people, there will no longer be blocks available to mine but the mining work will be needed to account for the transactions, so the creator will earn the corresponding commissions.

### Activity registration principle ###
The G1X units that will be put into circulation will be added to a public record that will be available on the Genexcoin home page. In this registry all units that are in circulation and those that are under protection will be found. G1X units will not be put up for sale until they are published in the aforementioned registry.

## Coin properties ##
- Name:               Genexcoin

- Abbreviation:       G1X

- Symbol:             ჵ

- Coin unit:          Gix

- Coin supply:        40,000,000 G1X and 4,000,000,000,000,000 Gix

- Algorithm:          SHA-256

- Block type:         Proof-of-Work

## Advanced properties ##
- Mineable:           No

- Decentralized:      No

- Public node:        No

- Stable:             Yes

- Backed asset:       Yes





